<div class="navigation" id="allCont">

	<div class="logobox">
		<a href="/"><img src="{{asset('/images/PPS1.jpg' ) }}"></a>
	</div>
		
	<nav id="navi">
		<ul>
			<li>
				<a href="/about">Apie mus</a>
			</li>

			<li>
				<a href="/gallery">Galerija</a>
			</li>

			<li>
				<a href="/members">Nariai</a>
			</li>

			<li>
				<a href="/news">Naujienos</a>
			</li>
			
			<li>
				<a href="/contacts">Kontaktai</a>
			</li>
		</ul>
	</nav>

	<button class="hamburger hamburger--spring" id="burgerino" type="button">
	  <span class="hamburger-box">
	    <span class="hamburger-inner"></span>
  	</span>
</button>
</div>


