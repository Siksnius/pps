
@extends('layouts.app')

@section('content')

		
<div class="heroContainer">

	<div class="maincontent"></div>
	<div class="sidebar">
		<div class="clndr-container">
		<div id="clndr">
		<div id="full-clndr" class="clearfix"> 
			<script type="text/template" id="full-clndr-template">
				<div class="clndr-controls">
	                <div class="clndr-btn clndr-previous-button"></div>
	  				<div class="clndr-month">
	  					<%= month %>
	  				</div>
	  				<div class="clndr-year">
	  					<%= year %>
	  				</div>
	                <div class="clndr-btn clndr-next-button"></div>
	            </div>
	            <div class="clndr-grid">
	                <div class="days-of-the-week">
	                	<div class="header-days">
			                <% _.each(daysOfTheWeek, function(day) { %>
			                    <div class="header-day"><%= day %></div>
			                <% }); %>
	            		</div>
	                    <div class="days">
	                    <% _.each(days, function(day) { %>
	                        <div class="<%= day.classes %>"><%= day.day %></div>
	                    <% }); %>
	                    </div>
	                </div>
	            </div>
			</script>
		</div>
	</div>
</div>
	

		<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ asset('js/underscore-min.js') }}"></script>
		<script src="{{ asset('js/moment.js') }}"></script>
		<script src="{{ asset('js/clndr.min.js') }}"></script>
		<script src="{{ asset('js/moment-with-locales.js') }}"></script>
		<script>
	
	// Locale nustatau ir surasau menesiu pavadinimus
	moment.locale('lt');

	moment.updateLocale('lt', {
	    months : [
	        'Sausis', 'Vasaris', 'Kovas', 'Balandis', 'Gegužė', 'Birželis', 'Liepa',
	        'Rugpjūtis', 'Rugsėjis', 'Spalis', 'Lapkritis', 'Gruodis'
	    ]
	});

	// sukuriu tuscia eventu masyva
	// just in case, jei nebus eventu ir erroru nebutu
	var events = [];

	$('#clndr').clndr({
	template: $('#full-clndr-template').html(),
	weekOffset: 1,
	daysOfTheWeek: ['S', 'P', 'A', 'T', 'K', 'P', 'Š'],
	numberOfRows: 5,
	events: events,
	clickEvents: {
	    click: function(target) {
	    	// pvz kas gali but, paspaudus ant evento boxo
	    	// is idejos tiesiog sukisu eventu sarasa i tam specialu box'a
	    	ev = $('.events-listing');

	    	if(target.events.length > 0) {
	    		var date = target.events[0].date;

	    		ev.addClass('events-listing-show');
	    		$('.close-listing').addClass('close-listing-show');
		    	ev.html('');
		    	ev.append('<h4>' + date + '</h4>');
		    	
		    	for(var i = 0; i < target.events.length; i++) {
		    		var link = '<a href="' + target.events[i].url + '" title="' + target.events[i].title + '">' + target.events[i].title + '</a>';
		    	
		    		ev.append('<span>' + link + '</span>');
			    }
		    	
	    	} else {
	    		ev.removeClass('events-listing-show');
	    		$('.close-listing').removeClass('close-listing-show');
	    	}
	    }
  	},
});
</script>
	</div>

</div>
   
@endsection
