@extends('layouts.app')

@section('content')

     <div class="titlebox">
     	<h1>Sąjungos Nariai</h1>	
         
     </div>

     <div class="all-members">

     	<div class="member">

     		<div class="logo">
     			
     		</div>

     		<div class="info">
     			<div class="all-info">
	     			<h1>UAB 'Litspringas'</h1>
	     			<p><span>ADRESAS:  </span>Salantų g. 8, Plungė, LT-90115</p>
	     			<p><span>TELEFONAS:  </span>8 448 46893</p>
	     			<p><span>EL. PAŠTAS:  </span>info@litspringas.com</p>
	     			<p><span>SVETAINĖ:  </span><a href="http://www.litspringas.com" target="_blank">www.litspringas.com</a> </p>
     			</div>
     		</div>
     	</div>


     	<div class="member">

     		<div class="logo logo2">
     			
     		</div>

     		<div class="info">
     			<div class="all-info">
	     			<h1>UAB 'Plungės lagūna' </h1>
	     			<p><span>ADRESAS: </span>Plungės g. 28, Rietavas, LT-90310</p>
	     			<p><span>TELEFONAS: </span>+370 448 68647</p>
	     			<p><span>EL. PAŠTAS: </span> info@plungeslaguna.lt</p>
	     			<p><span>SVETAINĖ: </span><a href="http://www.plungeslaguna.lt" target="_blank">www.plungeslaguna.lt</a> </p>
     			</div>
     		</div>
     	</div>

     	<div class="member">

     		<div class=" logo logo3">
     			
     		</div>

     		<div class="info">
     			<div class="all-info">
	     			<h1>UAB 'Plungės baldai' </h1>
	     			<p><span>ADRESAS: </span>Dariaus ir Girėno g. 38c, Plungė, LT-90118</p>
	     			<p><span>TELEFONAS: </span>+370 448 51115</p>
	     			<p><span>EL. PAŠTAS: </span>info@plungesbaldai.lt</p>
	     			<p><span>SVETAINĖ: </span><a href="http://www.plungesbaldai.com" target="_blank">www.plungesbaldai.com</a> </p>
     			</div>
     		</div>
     	</div>


          <div class="member">

               <div class="logo logo4">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Terekas' </h1>
                         <p><span>ADRESAS: </span>Vytauto g. 144a, Kretinga, LT-97133</p>
                         <p><span>TELEFONAS: </span>+370 445 51431</p>
                         <p><span>EL. PAŠTAS: </span>info@terekas.lt</p>
                         <p><span>SVETAINĖ: </span><a href="http://www.terekas.com" target="_blank">www.terekas.com</a> </p>
                    </div>
               </div>
          </div>


          <div class="member">

               <div class=" logo logo5">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>Plungės verslininkų klubas</h1>
                         <p><span>ADRESAS: </span>Vytauto g. 11, Plungė, LT-90123</p>
                         <p><span>TELEFONAS: </span></p>
                         <p><span>EL. PAŠTAS: </span></p>
                         
                    </div>
               </div>
          </div>



          <div class="member">

               <div class="logo logo6">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>VšĮ 'Bruneros' </h1>
                         <p><span>ADRESAS: </span>Palangos g. 30C, LT-97122 Kretinga</p>
                         <p><span>TELEFONAS: </span>+37044577355 </p>
                         <p><span>EL. PAŠTAS: </span></p>
                         <p><span>SVETAINĖ: </span><a href="http://bruneros.lt/" target="_blank">www.bruneros.lt/</a> </p>
                    </div>
               </div>
          </div>



          <div class="member">

               <div class="logo logo7"></div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Padagas ir ko' </h1>
                         <p><span>ADRESAS: </span>Birutės g.57b, Plungė, LT-90112</p>
                         <p><span>TELEFONAS: </span>+370 448 71863</p>
                         <p><span>EL. PAŠTAS: </span>info@padagas.lt</p>
                         <p><span>SVETAINĖ: </span><a href="https://www.padagas.lt" target="_blank">www.padagas.lt</a> </p>
                    </div>
               </div>
          </div>



          <div class="member">
          
               <div class="logo logo8">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Plungės Jonis' </h1>
                         <p><span>ADRESAS: </span>Telšių g. 39a, Plungė, LT-90163</p>
                         <p><span>TELEFONAS: </span>+370 682 19322</p>
                         <p><span>EL. PAŠTAS: </span>info@plungesjonis.lt</p>
                         <p><span>SVETAINĖ: </span><a href="http://www.plungesjonis.lt" target="_blank">www.plungesjonis.lt</a> </p>
                    </div>
               </div>
          </div>




          <div class="member">

               <div class="logo logo9">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Plungės kooperatinė prekyba' </h1>
                         <p><span>ADRESAS: </span>Birutės g. 50, Plungė, LT-90112</p>
                         <p><span>TELEFONAS: </span>+370 448 73170</p>
                         <p><span>EL. PAŠTAS: </span>info@vici.lt</p>
                         <p><span>SVETAINĖ: </span><a href="http://www.viciunaigroup.eu" target="_blank">www.viciunaigroup.eu</a> </p>
                    </div>
               </div>
          </div>



          <div class="member">

               <div class="logo logo10">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>Plungės technologijų ir verslo mokykla </h1>
                         <p><span>ADRESAS: </span>Mendeno g.7, Plungė, LT-90143</p>
                         <p><span>TELEFONAS: </span>8 448 71632</p>
                         <p><span>EL. PAŠTAS: </span>sekretore@plungestvm.lt</p>
                         <p><span>SVETAINĖ: </span><a href="http://plungestvm.lt" target="_blank">www.plungestvm.lt</a> </p>
                    </div>
               </div>
          </div>



          <div class="member">

               <div class="logo logo11">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Gilinis'</h1>
                         <p><span>ADRESAS: </span>Pramonės pr.4, Plungė, LT-90112</p>
                         <p><span>TELEFONAS: </span>+370 448 70026</p>
                         <p><span>EL. PAŠTAS: </span>info@gilinis.lt</p>
                         <p><span>SVETAINĖ: </span><a href="http://www.gilinis.lt" target="_blank">http://www.gilinis.lt</a> </p>
                    </div>
               </div>
          </div>



          <div class="member">

               <div class="logo logo12">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Constro'</h1>
                         <p><span>ADRESAS: </span>Lentpjūvės 7A, LT-090117, Plungė</p>
                         <p><span>TELEFONAS: </span>+370 677 22293</p>
                         <p><span>EL. PAŠTAS: </span>info@constro.eu</p>
                         <p><span>SVETAINĖ: </span><a href="http://www.constro.eu/lt/" target="_blank">http://www.constro.eu/lt/</a> </p>
                    </div>
               </div>
          </div>    

          <div class="member">

               <div class="logo logo13">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Hovden'</h1>
                         <p><span>ADRESAS: </span>Lentpjūvės 22, LT-090118, Plungė</p>
                         <p><span>TELEFONAS: </span>+37067099290</p>
                         <p><span>EL. PAŠTAS: </span></p> 
                         
                    </div>
               </div>
          </div>

         <h1 class="special-members">Garbės nariai</h1>

     
     
          <div class="member">

               <div class="logo logo14">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB ‘Alginora’ </h1>
                         <p><span>ADRESAS: </span>S.Nėries skg. 5 Plungė </p>
                         <p><span>TELEFONAS: </span>(8 448) 45097</p>
                         
                        
                    </div>
               </div>
          </div>


          <div class="member">

               <div class="logo logo15">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Plungės tekstilė' </h1>
                         <p><span>ADRESAS: </span>Telšių g. 26a, Plungė, LT-90163</p>
                         <p><span>TELEFONAS: </span>+370 686 22390   </p>
                         <p><span>EL. PAŠTAS: </span>ptextile@ptextile.lt</p>
                         <p><span>SVETAINĖ: </span><a href="http://www.ptextile.lt" target="_blank">www.ptextile.lt</a> </p>
                    </div>
               </div>
          </div>

          <div class="member">

               <div class="logo logo16">
                    
               </div>

               <div class="info">
                    <div class="all-info">
                         <h1>UAB 'Emega' </h1>
                         <p><span>ADRESAS: </span>Salantų g.11, Plungė, LT-90112</p>
                         <p><span>TELEFONAS: </span> 8448 72351  </p>
                         <p><span>EL. PAŠTAS: </span>emega@centras.lt</p>
                         
                    </div>
               </div>
          </div>


     </div>

   
@endsection