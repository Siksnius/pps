@extends('layouts.app')

@section('content')

<div class="heading">
	<div class="heading-box">
		<h1>Kontaktai</h1>
	</div>
		
</div>



<div class="contacts-box">
  
  

	<div class="all-infos">

    <div class="titlesside">
      <h5>Asociacijos pav.</h5>
      <h5>Prezidentas</h5>
      <h5>Adresas</h5>
      <h5>Tel.</h5>
      <h5>El. paštas</h5>
      <h5>Įmonės kodas</h5>
      <h5>A.s.</h5>
    </div>

    <div class="infoside">
      <p>Plungės rajono pramonininkų sąjunga</p>
      <p>Egidijus Rapalis</p>
      <p>Vytauto g. 7, Plungė, LT-90123</p>
      <p>+37063367717</p>
      <p>plunges.ps@gmail.com</p>
      <p>293295970</p>
      <p>LT724010043000091716 &nbsp;AB LUMINOR</p>
    </div> 

  </div>


	<div id="map"></div>

  <script>

      function initMap() {
        var myLatLng = {lat: 55.912892, lng: 21.843853};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYQ69_IVRs-dLE2N_N-NrfYYk7dBzicdc&callback=initMap">
    </script>
</div>

 

@endsection