@extends('admin.layouts.appadmin')

@section('content')


<hr/>
<h4 class="page-title">Posts</h4>
<hr/>
<a href="/admin/posts/create" class="btn btn-primary btn-sm" style="margin-bottom: 25px;">Create New</a>


<table class="table   align-middle">

	  <tr>
	  	<th>#</th>
	    <th>Title</th>
	    <th>Description</th> 
	    <th>Image</th>
	    <th>Action</th>
	  </tr>

	  @foreach($posts as $post)
		<tr>
		    <td>{{$post->id}}</td>
		    <td>{{$post->name}}</td> 
		    <td>{{$post->description}}</td>
		    <td>{{$post->image}}</td>
		    <td>
		    	
		    	
		    	<a class="btn btn-danger btn-sm" href="#">Delete</a>
		    	
		    </td>
	 	</tr>

	 	

	  @endforeach
	 
</table>




@endsection