<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('inc.head')

    
    <body>


    @include('inc.navbar')


    <div id="toppy" class="btnn">
		<p><i class="fas fa-arrow-up"></i></p>
	</div>

    @yield('content')




    @include('inc.footer')
        
    @include('inc.scripts')

       
 
    </body>
</html>