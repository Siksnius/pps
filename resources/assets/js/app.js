
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});



// buutton to go up script

var elem = document.getElementById('toppy');

elem.addEventListener('click', smoothscroll); 


function smoothscroll(){
	var curentScroll = document.documentElement.scrollTop;

	if (curentScroll > 0) {
		window.scrollTo(0, curentScroll - (curentScroll/10));
		window.requestAnimationFrame(smoothscroll);}

};

document.addEventListener('scroll', function() {
	var currentScroll = document.documentElement.scrollTop;

	if(currentScroll > 100) {
		elem.classList.add('showbox')
	} else {
		elem.classList.remove('showbox');
	}
});
// end .................................



// burger action script 

  var hamburger = document.querySelector(".hamburger");
  var navi = document.getElementById('navi')
  var allCont = document.getElementById('allCont')

  hamburger.addEventListener("click", function() {
    
    hamburger.classList.toggle("is-active");
    navi.classList.toggle("show-ul");
    allCont.classList.toggle("margin-bottom");
    
  });







