<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Session;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts= Post::orderBy('id', 'desc')->paginate(5);
        return view('admin.posts.index')->withPosts($posts);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

        $this->validate($request, [
            'name'=>'required|max:255',
            'description'=>'required',
            'image'=>'mimes:jpg,png,jpeg,gif,svg|max:2048'

        ]);

        $post = new Post;
        $post->name = $request->name;
        $post->description = $request->description;
        $post->image =$request->image;

        $post->save();

        return redirect()->route('posts.index');
     
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $post = Post::find($id);
        // return the view and pass in the var we previously created
        return view('posts.edit')->withPost($post);
         
         }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function update(Request $request, $id)
    {
        
        // VALIDATE THE DATA
            $this->validate($request, [
            'name'=>'required|max:255',
            'description'=>'required',
            'image'=>'mimes:jpg,png,jpeg,gif,svg|max:2048'

        ]);
        // SAVE DATA TO THE DATABASE 
            $post = Post::find($id);

            $post->name = $request->input('name');
            $post->description = $request->input('description');

            $post->save();
        // SET FLASH DATA WITH SUCCESS MESSAGE

             Session::flash('success', 'The post was succesfully saved!');

        // REDIRECT WITH FLASH TO POSTS. SHOW
             return redirect()->route('posts.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $post = Post::find($id);

        $post->delete();

        Session::flash('success', 'The post was succesfully deleted');

        return redirect()->route('posts.index');
    }
}
