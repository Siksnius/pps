<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about');

Route::get('/contacts', 'PagesController@contacts');

Route::get('/gallery', 'PagesController@gallery');

Route::get('/members', 'PagesController@members');

Route::get('/news', 'PagesController@news');



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');


Route::post('login', 'Auth\LoginController@login');


Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::group(['namespace' => 'admin', 'middleware' => 'auth', 'prefix' => 'admin'], function() {

	Route::get('/dashboard', 'PagesController@appadmin');

	Route::resource('posts', 'PostsController');

	// Route::resource('create', 'PostsController');





});